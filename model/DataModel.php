<?php

require 'app/ApiRequest.php';

class DataModel extends ApiRequest
{
    // Chemin absolue de l'API 'https://lecoledunumerique.fr/wp-json/wp/v2/'
    private $absoluteApiUrl;

    public function __construct($absoluteApiUrl)
    {
        $this->absoluteApiUrl = $absoluteApiUrl;
    }

    /**
     * Récupère les données de l'API en fonction du type demandé
     * 
     * @param string $type
     * @return object|array
     */
    public function getDataTrombinoscope($type)
    {
        $param = $this->getParams();
        switch ($type) {
            case "apprenants":
                $this->loadUrl("apprenants" . $param);
                break;
            case "promotions":
                $this->loadUrl("promotion");
                break;
            case "competences":
                $this->loadUrl("competence");
                break;
            default:
                break;
        }
        return $this->getData();
    }

    /**
     * Permet de passer l'URL à la classe parente
     * 
     * @param string $pathUrl
     * @return void
     */
    private function loadUrl($pathUrl)
    {
        $this->url = $this->absoluteApiUrl . $pathUrl;
    }

    /**
     * Permet de récupérer les paramètres du formulaire
     * et de retourner les données dans l'URL de l'ApiRequest
     * 
     * @return string
     */
    private function getParams()
    {
        $param = "?per_page=100";

        if(!empty($_GET)) {
            if (isset($_GET['promotion']) && !empty($_GET['promotion']) && $_GET['promotion'] != 0) {
                $param .= $param === "" ? "?" : "&";
                $param .= "Promotion=" . $this->encodeUrl($_GET['promotion']);
            }

            if (isset($_GET['competences']) && !empty($_GET['competences'])) {
                foreach ($_GET['competences'] as $key => $competence) {
                    if ($key === 0) {
                        $param .= $param === "" ? "?" : "&";
                        $param .= "Competence=" . $this->encodeUrl($competence);
                    } else {
                        $param .= "," . $this->encodeUrl($competence);
                    }
                }
            }

            if (isset($_GET['name']) && !empty($_GET['name'])) {
                $param .= $param === "" ? "?" : "&";
                $param .= "search=" . $this->encodeUrl($_GET['name']);
            }
        }
        return $param;
    }
}
