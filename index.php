<?php
require 'app/TemplateEngine.php';
require 'model/DataModel.php';
require 'controller/PageController.php';


$templateEngine = new TemplateEngine('view/templates');
$dataModel = new DataModel("https://www.lecoledunumerique.fr/wp-json/wp/v2/");
$page = new PageController($templateEngine, $dataModel);

echo $templateEngine->render('header.php');

echo $page->renderFilterForm();
echo $page->renderTrombinoscopeList();

echo $templateEngine->render("footer.php");