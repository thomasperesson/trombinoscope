<?php

// class Card {
//     public function __construct($url) {
//         $this->url = $url;
//         $this->apprenantsModel = new ApprenantsModel($this->url);
//         $this->apprenants = $this->apprenantsModel->getApprenants();
//     }

//     public function render() {

//     }
// }

// $apprenantsModel = new ApprenantsModel();
// $apprenants = $apprenantsModel->getApprenants();
?>
<div id="liste-apprenants">
    <?php if (isset($apprenants) && !empty($apprenants)) : ?>
        <?php foreach ($apprenants as $apprenant) : ?>
            <?php // echo '<pre>';print_r($apprenant); echo '</pre>'; 
            ?>
            <?php $content = '' ?>
            <div class="card-container">
                <div class="card-header-container">
                    <p class="card-title">
                        <?php printf('%s', $apprenant['prenom']); ?>
                        <span class="text-yellow ff-racing"><?php printf('%s', ($apprenant['nom'])); ?></span>
                    </p>
                    <?php
                    $competences = "";
                    if (array_key_exists('id', $apprenant['competences'][0])) : ?>
                        <?php $competences = $apprenant['competences']; ?>
                        <div class="skills-container">
                            <?php foreach ($competences as $key => $competence) : ?>
                                <span class="skills"><?php printf('%s', $competence['name']); ?></span>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                    <div class="promotion text-yellow">
                        <?php printf('%s', strtoupper(str_replace('romotion-', '', $apprenant['promotion']['slug'])));  ?>
                    </div>
                </div>
                <div class="img-container">
                    <img class="img" src="<?php printf('%s', $apprenant['image']); ?>" alt="<?php printf('%s', $apprenant['title']['rendered']); ?>">
                </div>
                <div class="links-container">
                    <?php if ($apprenant['portfolio'] !== "") : ?>
                        <a id="portfolio" href="<?php printf('%s', $apprenant['portfolio']); ?>" target="_blank"><i class="far fa-id-badge"></i></a>
                    <?php endif; ?>
                    <?php if ($apprenant['linkedin'] !== "") : ?>
                        <a id="linkedin" href="<?php printf('%s', $apprenant['linkedin']); ?>" target="_blank"><i class="fab fa-linkedin"></i></a>
                    <?php endif; ?>
                    <?php if (array_key_exists('cv', $apprenant)) : ?>
                        <a id="cv" href="<?php printf('%s', $apprenant['cv']); ?>" target="_blank"><i class="fas fa-file-download"></i></a>
                    <?php endif; ?>
                </div>
                <div class="show-excerpt"><i class="fas fa-chevron-right"></i></div>
                <div class="excerpt">
                    <h3 class="excerpt-title ff-racing">Résumé</h3>
                    <?php printf('%s', $apprenant['excerpt']['rendered']); ?>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else : ?>
        <p class="no-results">Aucun résultats trouvés</p>
    <?php endif; ?>
</div>