<?php

// $promotionsModel = new PromotionsModel();
// $promotions = $promotionsModel->getPromotions();

// $competencesModel = new CompetencesModel();
// $competences = $competencesModel->getCompetences();
?>


<form id="filterBar" method="GET">
    <p class="filter-text text-blue ff-racing">Filtres</p>
    <div class="search-select-group">
        <div class="promotion-selector-group">
            <select name="promotion" id="promotion">
                <option value="0">Toutes</option>
                <?php foreach ($promotions as $promotion) : ?>
                    <option value="<?php printf('%d', $promotion['id']) ?>"><?php printf('%s', $promotion['name']); ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="search-bar-group">
            <input type="search" name="name" id="name" placeholder="Recherche">
            <button class="searchButton"><i class="fas fa-search"></i></button>
        </div>
    </div>
    <div class="skills-group">
        <?php foreach ($competences as $competence) : ?>
            <div class="checkbox-label-group">
                <input class="inputSkill" type="checkbox" name="competences[]" id="skill-<?php printf('%d', $competence['id']); ?>" value="<?php printf('%s', $competence['id']); ?>" />
                <label for="skill-<?php printf('%d', $competence['id']); ?>"><?php printf('%s', $competence['name']); ?></label>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="submit-button-container">
        <button class="submit-button ff-racing">Filtrer</button>
    </div>
</form>