<?php

class PageController
{
    public function __construct(TemplateEngine $templateEngine, DataModel $dataModel)
    {
        $this->templateEngine = $templateEngine;
        $this->dataModel = $dataModel;
    }

    public function renderTrombinoscopeList()
    {
        try {
            $apprenantsData = $this->dataModel->getDataTrombinoscope("apprenants");
            return $this->templateEngine->render("card.php", ['apprenants' => $apprenantsData]);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function renderFilterForm()
    {
        try {
            $competencesData = $this->dataModel->getDataTrombinoscope("competences");
            $promotionsData = $this->dataModel->getDataTrombinoscope("promotions");
            return $this->templateEngine->render("filterBar.php", ['promotions' => $promotionsData, 'competences' => $competencesData]);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
}
